package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.model.Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public void add(Task task) {
        list.add(task);
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task: list) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        List<Task> taskList = new ArrayList<>();
        for (final Task task: list) {
            if (projectId.equals(task.getProjectId())) taskList.add(task);
        }
        return taskList;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return list.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task: list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public List<Task> removeAllByProjectId(String projectId) {
        List<Task> taskList = new ArrayList<>();
        Iterator<Task> iterator = list.iterator();
        while (iterator.hasNext()) {
            Task task = iterator.next();
            if (projectId.equals(task.getProjectId())) {
                taskList.add(task);
                iterator.remove();
            }
        }
        return taskList;
    }

    @Override
    public Task bindTaskPyProjectId(String projectId, String taskId) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(String taskId) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId("");
        return task;
    }

    @Override
    public int size() {
        return list.size();
    }

}
