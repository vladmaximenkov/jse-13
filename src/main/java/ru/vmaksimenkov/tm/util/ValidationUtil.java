package ru.vmaksimenkov.tm.util;

import ru.vmaksimenkov.tm.enumerated.Status;

public interface ValidationUtil {

    static boolean isEmpty(final String value) { return value == null || value.isEmpty(); }

    static boolean checkIndex(final int index, int size) {
        if (index < 0) return false;
        return index <= size - 1;
    }

    static boolean checkStatus(final String status){
        for (Status s : Status.values()) {
            if (s.name().equals(status)) {
                return true;
            }
        }
        return false;
    }

}
