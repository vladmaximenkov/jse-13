package ru.vmaksimenkov.tm.service;

import ru.vmaksimenkov.tm.api.repository.IProjectRepository;
import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.api.service.IProjectTaskService;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.model.Task;

import static ru.vmaksimenkov.tm.util.ValidationUtil.*;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    final private ITaskRepository taskRepository;

    final private IProjectRepository projectRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(String projectId) {
        if (isEmpty(projectId) || taskRepository.findAllByProjectId(projectId).size() < 1) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task bindTaskByProjectId(String projectId, String taskId) {
        if (isEmpty(projectId) || isEmpty(taskId) || projectRepository.findOneById(projectId) == null) return null;
        return taskRepository.bindTaskPyProjectId(projectId, taskId);
    }

    @Override
    public Task unbindTaskFromProject(String taskId) {
        if (isEmpty(taskId)) return null;
        return taskRepository.unbindTaskFromProject(taskId);
    }

    @Override
    public Project removeProjectById(String id) {
        if (isEmpty(id) || projectRepository.findOneById(id) == null) return null;
        taskRepository.removeAllByProjectId(id);
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project removeProjectByName(String name) {
        if (isEmpty(name)) return null;
        final Project project = projectRepository.findOneByName(name);
        if (project == null) return null;
        taskRepository.removeAllByProjectId(project.getId());
        return projectRepository.removeOneById(project.getId());
    }

    @Override
    public Project removeProjectByIndex(Integer index) {
        if (!checkIndex(index, projectRepository.size())) return null;
        final Project project = projectRepository.findOneByIndex(index);
        if (project == null) return null;
        taskRepository.removeAllByProjectId(project.getId());
        return projectRepository.removeOneById(project.getId());
    }
}
