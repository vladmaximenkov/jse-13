package ru.vmaksimenkov.tm.service;

import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.api.service.ITaskService;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.model.Task;

import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.*;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Integer showList() {
        final List<Task> tasks = findAll();
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        return tasks.size();
    }

    @Override
    public Task add(final String name, final String description) {
        if (isEmpty(name) || isEmpty(description)) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (!checkIndex(index, taskRepository.size())) return null;
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneByName(final String name) {
        if (isEmpty(name)) return null;
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task findOneById(final String id) {
        if (isEmpty(id)) return null;
        return taskRepository.findOneById(id);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        if (!checkIndex(index, taskRepository.size())) return null;
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task removeOneByName(final String name) {
        if (isEmpty(name)) return null;
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task removeOneById(final String id) {
        if (isEmpty(id)) return null;
        return  taskRepository.removeOneById(id);
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description){
        if (isEmpty(id) || isEmpty(name)) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByName(final String name, final String name_new, final String description){
        if (isEmpty(name) || isEmpty(name_new)) return null;
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setName(name_new);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (!checkIndex(index, taskRepository.size())) return null;
        if (isEmpty(name)) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(String id) {
        if (isEmpty(id)) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(String name) {
        if (isEmpty(name)) return null;
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(Integer index) {
        if (!checkIndex(index, taskRepository.size())) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(String id) {
        if (isEmpty(id)) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(String name) {
        if (isEmpty(name)) return null;
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(Integer index) {
        if (!checkIndex(index, taskRepository.size())) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task setTaskStatusById(String id, Status status) {
        if (isEmpty(id)) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task setTaskStatusByName(String name, Status status) {
        if (isEmpty(name)) return null;
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task setTaskStatusByIndex(Integer index, Status status) {
        if (!checkIndex(index, taskRepository.size())) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }
}
