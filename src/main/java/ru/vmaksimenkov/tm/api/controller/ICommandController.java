package ru.vmaksimenkov.tm.api.controller;

public interface ICommandController {

    void sysExit();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showWelcome();

    void showSystemInfo();

}
