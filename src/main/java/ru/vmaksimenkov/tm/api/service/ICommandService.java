package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
