package ru.vmaksimenkov.tm.controller;

import ru.vmaksimenkov.tm.api.controller.ITaskController;
import ru.vmaksimenkov.tm.api.service.IProjectService;
import ru.vmaksimenkov.tm.api.service.IProjectTaskService;
import ru.vmaksimenkov.tm.api.service.ITaskService;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkStatus;
import static ru.vmaksimenkov.tm.util.TerminalUtil.dashedLine;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public TaskController(final ITaskService taskService, final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        if (taskService.showList() < 1) System.out.println("[NO TASKS]");
        else System.out.println("[OK]");
    }

    @Override
    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
        System.out.println("PROJECT ID: " + task.getProjectId());
        System.out.print(dashedLine());
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showListByProjectId() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        if (projectService.showList() < 1) {
            System.out.println("[FAIL! NO PROJECTS]");
            return;
        }
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> taskList = projectTaskService.findAllTaskByProjectId(projectId);
        if (taskList == null) System.out.println("[FAIL! NO SUCH PROJECT]");
        else if (taskList.size() < 1) System.out.println("[OK, NO TASKS]");
        else {
            for (Task task : taskList) showTask(task);
            System.out.println("[OK]");
        }
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeOneByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByIndex(index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskByName() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW NAME:");
        final String name_new = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByName(name, name_new, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startTaskById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startTaskByName() {
        System.out.println("[START TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startTaskByName(name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.startTaskByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void finishTaskById() {
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void finishTaskByName() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskByName(name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void finishTaskByIndex() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.finishTaskByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void setTaskStatusById() {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        if (!checkStatus(statusId)) {
            System.out.println("[FAIL! NO SUCH STATUS]");
            return;
        }
        final Status status = Status.valueOf(statusId);
        final Task taskUpdated = taskService.setTaskStatusById(id, status);
        if (taskUpdated == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void setTaskStatusByName() {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        if (!checkStatus(statusId)) {
            System.out.println("[FAIL! NO SUCH STATUS]");
            return;
        }
        final Status status = Status.valueOf(statusId);
        final Task taskUpdated = taskService.setTaskStatusByName(name, status);
        if (taskUpdated == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void setTaskStatusByIndex() {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        if (!checkStatus(statusId)) {
            System.out.println("[FAIL! NO SUCH STATUS]");
            return;
        }
        final Status status = Status.valueOf(statusId);
        final Task taskUpdated = taskService.setTaskStatusByIndex(index, status);
        if (taskUpdated == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void bindTaskByProjectId() {
        System.out.println("[BIND TASK TO PROJECT BY ID]");
        if (taskService.showList() < 1){
            System.out.println("[FAIL! NO TASKS]");
            return;
        }
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("PROJECTS LIST:");
        if (projectService.showList() < 1) {
            System.out.println("[FAIL! NO PROJECTS]");
            return;
        }
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final Task taskUpdated = projectTaskService.bindTaskByProjectId(projectId, task.getId());
        if (taskUpdated == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void unbindTaskById() {
        System.out.println("[UNBIND TASK BY ID]");
        if (taskService.showList() < 1){
            System.out.println("[FAIL! NO TASKS]");
            return;
        }
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
